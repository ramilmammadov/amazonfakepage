<meta name="layout" content="mainAdmin"/>

<div class="col-md-8 col-md-offset-2">
    <g:uploadForm action="saveReview" id="${review?.id}">
        <div class="form-group">
            <label>Title:</label>
            <input class="form-control" name="title" type="text" value="${review?.title}"/>
        </div>

        <div class="form-group">
            <label>Title 1:</label>
            <input class="form-control" name="title1" type="text" value="${review?.title1 ?: 'Kundenrezension'}"/>
        </div>

        <div class="form-group">
            <label>Name:</label>
            <input class="form-control" name="name" type="text" value="${review?.name}"/>
        </div>

        <div class="form-group">
            <label>Name link:</label>
            <input class="form-control" name="nameLink" type="text" value="${review?.nameLink}"/>
        </div>

        <div class="form-group">
            <label>Star count 1:</label>
            <input class="form-control" name="startCount" type="number" value="${review?.startCount}"/>
        </div>

        <div class="form-group">
            <label>Date:</label>
            <input class="form-control" name="date" type="text" value="${review?.date}"/>
        </div>

        <div class="form-group">
            <label>Title2:</label>
            <input class="form-control" name="title2" type="text" value="${review?.title2}"/>
        </div>

        <div class="form-group">
            <label>Color:</label>
            <input class="form-control" name="color" type="text" value="${review?.color}"/>
        </div>

        <div class="form-group">
            <label>Color Link:</label>
            <input class="form-control" name="colorLink" type="text" value="${review?.colorLink}"/>
        </div>

        <div class="form-group">
            <label>Verified text:</label>
            <input class="form-control" name="verified" type="text"
                   value="${review?.verified ?: 'Verifizierter Kauf'}"/>
        </div>

        <div class="form-group">
            <label>Review content:</label>
            <textarea class="form-control" name="reviewContent">
                ${review?.reviewContent}
            </textarea>
        </div>

        <div class="form-group">
            <label>Product Title:</label>
            <input class="form-control" name="productTitle" type="text" value="${review?.productTitle}"/>
        </div>

        <div class="form-group">
            <label>Product Title link:</label>
            <input class="form-control" name="productTitleLink" type="text" value="${review?.productTitleLink}"/>
        </div>

        <div class="form-group">
            <label>By whom:</label>
            <input class="form-control" name="byWhom" type="text" value="${review?.byWhom}"/>
        </div>

        <div class="form-group">
            <label>By whom link:</label>
            <input class="form-control" name="byWhomLink" type="text" value="${review?.byWhomLink}"/>
        </div>

        <div class="form-group">
            <label>Star count:</label>
            <input class="form-control" name="starCount" type="text" value="${review?.starCount ?: '5'}"/>
        </div>

        <div class="form-group">
            <label>Customer Ratings Text:</label>
            <input class="form-control" name="customerRatingsText" type="text" value="${review?.customerRatingsText}"/>
        </div>

        <div class="form-group">
            <label>5 star percent:</label>
            <input class="form-control" name="star5Perc" type="number" value="${review?.star5Perc}"/>
        </div>

        <div class="form-group">
            <label>4 star percent:</label>
            <input class="form-control" name="star4Perc" type="number" value="${review?.star4Perc}"/>
        </div>

        <div class="form-group">
            <label>3 star percent:</label>
            <input class="form-control" name="star3Perc" type="number" value="${review?.star3Perc}"/>
        </div>

        <div class="form-group">
            <label>2 star percent:</label>
            <input class="form-control" name="star2Perc" type="number" value="${review?.star2Perc}"/>
        </div>

        <div class="form-group">
            <label>1 star percent:</label>
            <input class="form-control" name="star1Perc" type="number" value="${review?.star1Perc}"/>
        </div>

        <div class="form-group">
            <label>Price:</label>
            <input class="form-control" name="price" type="text" value="${review?.price ?: ' €'}"/>
        </div>

        <div class="form-group">
            <label>Shipping Info:</label>
            <input class="form-control" name="shippingInfo" type="text" value="${review?.shippingInfo}"/>
        </div>


        <div class="form-group">
            <label>Review Images:</label>
            <input multiple class="form-control" name="reviewImages" type="file"/>
        </div>

        <div class="form-group">
            <label>Product Image:</label>
            <input class="form-control" name="productImages" type="file"/>
        </div>

        <div class="form-group">
            <label>Helpful votes:</label>;
            <input class="form-control" name="helpfulVotes" required type="number"
                   value="${review?.helpfulVotes ?: 0}"/>
        </div>

        <div class="form-group">
            <label>Path:</label>
            <input class="form-control" name="path" value="${review?.path}"/>
        </div>

        <div class="form-group">
            <button class="btn btn-primary pull-right" type="submit">Save</button>
        </div>
    </g:uploadForm>
</div>