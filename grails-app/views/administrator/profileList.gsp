<meta name="layout" content="mainAdmin"/>

<div class="col-md-8 col-md-offset-2">
    <table class="table table-bordered">
        <thead>
        <th>Image</th>
        <th>Full Name</th>
        <th>Ranking</th>
        <th>Edit</th>
        <th>Delete</th>
        </thead>
        <tbody>
        <g:each in="${profiles}" var="profile">
            <tr>
                <td>
                    <g:link controller="home" action="profile" params="[path: profile?.path]">
                        <img src="${createLink(controller: 'imageDisplayer', action: 'index', params: [photoName: profile.image])}"
                             style="width: 100px;height: 100px"/>
                    </g:link>
                </td>
                <td>${profile?.name}</td>
                <td>${profile?.ranking}</td>
                <td>
                    <g:link action="addProfile" id="${profile?.id}">
                        <img class="oper-icon" src="${resource(dir: 'icon', file: 'edit.svg')}"/>
                    </g:link>
                </td>
                <td>
                    <g:link action="deleteProfile" id="${profile?.id}">
                        <img class="oper-icon" src="${resource(dir: 'icon', file: 'delete.svg')}"/>
                    </g:link>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>

</div>

<g:link action="addProfile" class="add">
    <img src="${resource(dir: 'icon', file: 'add.svg')}"/>
</g:link>