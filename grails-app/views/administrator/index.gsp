<meta name="layout" content="mainAdmin"/>

<table class="table table-bordered">
    <thead>
    <th>Review</th>
    <th>Edit</th>
    <th>Delete</th>
    </thead>
    <tbody>
    <g:each in="${list}" var="review">
        <tr>
            <td>
                <a href="${createLink(controller: 'home', action: 'info', id: review?.path)}">${review?.title}</a>
            </td>
            <td>
                <a href="${createLink(action: 'add', id: review?.id)}">
                    Edit
                </a>
            </td>
            <td>
                <a style="color:red" href="${createLink(action: 'delete', id: review?.id)}">
                    Delete
                </a>
            </td>
        </tr>
    </g:each>

    </tbody>
</table>