<meta name="layout" content="mainAdmin"/>

<div class="col-md-8 col-md-offset-2">
    <g:form action="saveConfigParams">
        <div class="form-group">
            <label>Title:</label>
            <input class="form-control" name="title" type="text"
                   value="${configParams?.find { it.k.equals('title') }?.v}"/>
        </div>

        <div class="form-group">
            <label>Favicon:</label>
            <input class="form-control" name="favicon" type="text"
                   value="${configParams?.find { it.k.equals('favicon') }?.v}"/>
        </div>

        <div class="form-group">
            <label>Logo:</label>
            <input class="form-control" name="logo" type="text"
                   value="${configParams?.find { it.k.equals('logo') }?.v}"/>
        </div>

        <div class="form-group">
            <label>Show price line:</label>
            <input type="checkbox" name="showPrice"
                   value="1"
                   <g:if test="${configParams?.find { it.k.equals('showPrice') }?.v?.equals('1')}">checked</g:if>/>
        </div>
        <hr/>

        <g:each in="${1..20}" var="num">
            <div class="col-md-12">
                <div class="col-md-6">
                    <label>Navbar title:</label>
                    <input class="form-control" name="navTitle${num}" type="text"
                           value="${configParams?.find { it.k.equals('navTitle' + num) }?.v}"/>
                </div>

                <div class="col-md-6">
                    <label>Navbar path:</label>
                    <input class="form-control" name="navLink${num}" type="text"
                           value="${configParams?.find { it.k.equals('navLink' + num) }?.v}"/>
                </div>
            </div>

        </g:each>


        <div class="form-group" style="margin-top: 650px;margin-bottom: 700px">
            <button class="btn btn-primary pull-right" type="submit">Save</button>
        </div>
    </g:form>
</div>