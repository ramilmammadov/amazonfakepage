<meta name="layout" content="mainAdmin"/>

<div class="col-md-8 col-md-offset-2">
    <g:uploadForm action="saveProfile" id="${profile?.id}">
        <div class="form-group">
            <label>Full name:</label>
            <input class="form-control" name="name" type="text" value="${profile?.name}"/>
        </div>

        <div class="form-group">
            <label>Ranking:</label>
            <input class="form-control" name="ranking" type="text" value="${profile?.ranking}"/>
        </div>

        <div class="form-group">
            <label>Reviews:</label> <br/>
            <g:each in="${rovshenamazon.Review?.findAll([sort: 'id', order: 'desc', max: 1000])}" var="r">
                <div class="input-group">
                    <span class="input-group-addon">
                        <input <g:if test="${profile?.profileReviews?.find { it.review.id == r.id }}">checked</g:if>
                               type="checkbox"
                               name="review" value="${r.id}"/>
                    </span>

                    <p class="form-control">
                        <a href="${createLink(controller: 'home', action: 'info', id: r?.path)}">${r.title}</a>
                    </p>
                </div> <br/>
            </g:each>
        </div>
        <div class="form-group">
            <label>Path:</label>
            <input class="form-control" required name="path" value="${profile?.path}"/>
        </div>
        <div class="form-group">
            <label>Image:</label>
            <input class="form-control" name="image" type="file"/>
        </div>


        <div class="form-group">
            <button class="btn btn-primary pull-right" type="submit">Save</button>
        </div>
    </g:uploadForm>
</div>