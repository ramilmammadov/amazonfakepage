
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
	<title>Seite wurde nicht gefunden</title>

	<style type="text/css">
	a:link { font-family: verdana,arial,helvetica,sans-serif; color: #004B91; }
	a:visited { font-family: verdana,arial,helvetica,sans-serif; color: #996633; }
	a:active { font-family: verdana,arial,helvetica,sans-serif; color: #FF9933; }
	</style>
</head>
<body bgcolor="#FFFFFF" text="#000000" style="font-family: verdana,arial,helvetica,sans-serif;">
<!--
            Um über den automatisierten Zugriff auf Amazon-Daten zu sprechen, wenden Sie sich bitte an api-services-support@amazon.com.

Weitere Informationen über die Migration zu unseren APIs finden Sie in unseren Marketplace APIs unter https://developer.amazonservices.com/ref=rm_5_sv bzw. in unserer Product Advertising API, wenn es sich um Anwendungsfälle im Rahmen von Werbung handelt: https://affiliate-program.amazon.com/gp/advertising/api/detail/main.html/ref=rm_5_ac.
        -->
<center>
	<a href="/ref=cs_404_logo">
		<img src="https://images-eu.ssl-images-amazon.com/images/G/03/ShoppingPortal/logo._CB485946978_.png"
			 alt="Amazon" border="0"/>
	</a>
	<table border="0" align="center" style="margin-top: 20px;">
		<tr>
			<td>
				<img src="https://images-eu.ssl-images-amazon.com/images/G/03/x-locale/common/kailey-kitty._CB485935146_.gif" width="40" height="35" border="0">
			</td>
			<td>
				<b style="color:#E47911">Suchen Sie etwas?</b>
				<br/>
				Tut uns leid.  Die eingegebene Webadresse ist keine funktionsfähige Seite auf unserer Website.
				<br/><br/>
				<img src="https://images-eu.ssl-images-amazon.com/images/G/03/x-locale/common/orange-arrow._CB485934058_.gif" width="10" height="9" border="0">
				<b><a href="/ref=cs_404_link">Klicken Sie hier, um zur Amazon-Startseite zurückzukehren.</a></b>
			</td>
		</tr>
	</table>
</center>
</body>
</html>
