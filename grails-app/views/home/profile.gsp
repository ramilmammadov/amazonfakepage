<!doctype html>
<html lang="de-de" class="a-no-js" data-19ax5a9jf="dingo">
<!-- sp:feature:head-start -->
<head>
    <script>var aPageStart = (new Date()).getTime();</script>
    <meta charset="utf-8"/> <script type='text/javascript'>var ue_t0 = ue_t0 || +new Date();</script>
    <!-- sp:feature:cs-optimization --> <meta http-equiv='x-dns-prefetch-control' content='on'>
    <link rel="dns-prefetch" href="https://images-eu.ssl-images-amazon.com">
    <link rel="dns-prefetch" href="https://m.media-amazon.com">
    <link rel="dns-prefetch" href="https://completion.amazon.com">
    <link rel="stylesheet" type="text/css" href="https://m.media-amazon.com/images/G/02/CustomerProfileWebsiteReactAssets/CustomerProfileWebsiteReactAssets-1.1.1111.0/customer-profile-website._V433026306_.css" />
    <link
            rel="shortcut icon"
            href="${rovshenamazon.ConfigParams?.findByK('favicon').v}"
            type="image/x-icon"/>
    <script type='text/javascript'> window.ue_ihb = (window.ue_ihb || window.ueinit || 0) + 1;
    if (window.ue_ihb === 1) {
        var ue_csm = window, ue_hob = +new Date();
        (function (d) {
            var e = d.ue = d.ue || {}, f = Date.now || function () {
                return +new Date
            };
            e.d = function (b) {
                return f() - (b ? 0 : d.ue_t0)
            };
            e.stub = function (b, a) {
                if (!b[a]) {
                    var c = [];
                    b[a] = function () {
                        c.push([c.slice.call(arguments), e.d(), d.ue_id])
                    };
                    b[a].replay = function (b) {
                        for (var a; a = c.shift();) b(a[0], a[1], a[2])
                    };
                    b[a].isStub = 1
                }
            };
            e.exec = function (b, a) {
                return function () {
                    try {
                        return b.apply(this, arguments)
                    } catch (c) {
                        ueLogError(c, {attribution: a || "undefined", logLevel: "WARN"})
                    }
                }
            }
        })(ue_csm);
        var ue_err_chan = 'jserr-rw';
        (function (d, e) {
            function h(f, b) {
                if (!(a.ec > a.mxe) && f) {
                    a.ter.push(f);
                    b = b || {};
                    var c = f.logLevel || b.logLevel;
                    c && c !== k && c !== m && c !== n && c !== p || a.ec++;
                    c && c != k || a.ecf++;
                    b.pageURL = "" + (e.location ? e.location.href : "");
                    b.logLevel = c;
                    b.attribution = f.attribution || b.attribution;
                    a.erl.push({ex: f, info: b})
                }
            }

            function l(a, b, c, e, g) {
                d.ueLogError({
                    m: a,
                    f: b,
                    l: c,
                    c: "" + e,
                    err: g,
                    fromOnError: 1,
                    args: arguments
                }, g ? {attribution: g.attribution, logLevel: g.logLevel} : void 0);
                return !1
            }

            var k = "FATAL", m = "ERROR", n = "WARN", p = "DOWNGRADED", a = {
                ec: 0, ecf: 0, pec: 0, ts: 0, erl: [], ter: [], mxe: 50, startTimer: function () {
                    a.ts++;
                    setInterval(function () {
                        d.ue && a.pec < a.ec && d.uex("at");
                        a.pec = a.ec
                    }, 1E4)
                }
            };
            l.skipTrace = 1;
            h.skipTrace = 1;
            h.isStub = 1;
            d.ueLogError = h;
            d.ue_err = a;
            e.onerror = l
        })(ue_csm, window);
        var ue_id = '51BDYV5GTX3ZFDYJ0XZ7', ue_url = '/gp/uedata', ue_navtiming = 1, ue_mid = 'A1PA6795UKMFR9',
            ue_sid = '262-7091204-2937867', ue_sn = 'www.amazon.de', ue_furl = 'fls-eu.amazon.de',
            ue_surl = 'https://unagi-eu.amazon.com/1/events/com.amazon.csm.nexusclient.prod', ue_int = 0, ue_fcsn = 1,
            ue_urt = 3, ue_rpl_ns = 'cel-rpl', ue_ddq = 1,
            ue_fpf = '//fls-eu.amazon.de/1/batch/1/OP/A1PA6795UKMFR9:262-7091204-2937867:51BDYV5GTX3ZFDYJ0XZ7$uedata=s:',
            ue_sbuimp = 1, ue_swi = 1;

        function ue_viz() {
            (function (c, e, a) {
                function k(b) {
                    if (c.ue.viz.length < p && !l) {
                        var a = b.type;
                        b = b.originalEvent;
                        /^focus./.test(a) && b && (b.toElement || b.fromElement || b.relatedTarget) || (a = e[m] || ("blur" == a || "focusout" == a ? "hidden" : "visible"), c.ue.viz.push(a + ":" + (+new Date - c.ue.t0)), "visible" == a && (ue.isl && uex("at"), l = 1))
                    }
                }

                for (var l = 0, f, g, m, n = ["", "webkit", "o", "ms", "moz"], d = 0, p = 20, h = 0; h < n.length && !d; h++) if (a = n[h], f = (a ? a + "H" : "h") + "idden", d = "boolean" == typeof e[f]) g = a + "visibilitychange", m = (a ? a + "V" : "v") + "isibilityState";
                k({});
                d && e.addEventListener(g, k, 0);
                c.ue && d && (c.ue.pageViz = {event: g, propHid: f})
            })(ue_csm, document, window)
        };(function (d, k, J) {
            function C(a) {
                return a && a.replace && a.replace(/^\s+|\s+$/g, "")
            }

            function q(a) {
                return "undefined" === typeof a
            }

            function D(a, b) {
                for (var c in b) b[r](c) && (a[c] = b[c])
            }

            function E(a) {
                try {
                    var b = J.cookie.match(RegExp("(^| )" + a + "=([^;]+)"));
                    if (b) return b[2].trim()
                } catch (c) {
                }
            }

            function K(p, b) {
                p && (d.ue_id = a.id = a.rid = p, t = t.replace(/((.*?:){2})(\w+)/, function (a, b) {
                    return b + p
                }));
                b && (t = t.replace(/(.*?:)(\w|-)+/, function (a, d) {
                    return d + b
                }), d.ue_sid = b);
                d.ue_fpf = t;
                d.ue.tag("accelerated-experience")
            }

            function L() {
                var a = {};
                return function (b) {
                    b && (a[b] = 1);
                    b = [];
                    for (var c in a) a[r](c) && b.push(c);
                    return b
                }
            }

            function u(d, b, c, e) {
                e = e || +new z;
                var f, k;
                if (b || q(c)) {
                    if (d) for (k in f = b ? h("t", b) || h("t", b, {}) : a.t, f[d] = e, c) c[r](k) && h(k, b, c[k]);
                    return e
                }
            }

            function h(d, b, c) {
                var e = b && b != a.id ? a.sc[b] : a;
                e || (e = a.sc[b] = {});
                "id" === d && c && (N = 1);
                return e[d] = c || e[d]
            }

            function O(d, b, c, e, f) {
                c = "on" + c;
                var h = b[c];
                "function" === typeof h ? d && (a.h[d] = h) : h = function () {
                };
                b[c] = function (a) {
                    f ? (e(a), h(a)) : (h(a), e(a))
                };
                b[c] && (b[c].isUeh = 1)
            }

            function P(p, b, c, e) {
                function w(b, c) {
                    var d = [b], g = 0, e = {}, f, k;
                    c ? (d.push("m=1"), e[c] = 1) : e = a.sc;
                    for (k in e) if (e[r](k)) {
                        var l = h("wb", k), m = h("t", k) || {}, w = h("t0", k) || a.t0, n;
                        if (c || 2 == l) {
                            l = l ? g++ : "";
                            d.push("sc" + l + "=" + k);
                            for (n in m) 3 >= n.length && !q(m[n]) && null !== m[n] && d.push(n + l + "=" + (m[n] - w));
                            d.push("t" + l + "=" + m[p]);
                            if (h("ctb", k) || h("wb", k)) f = 1
                        }
                    }
                    !F && f && d.push("ctb=1");
                    return d.join("&")
                }

                function M(b, c, g, e) {
                    if (b) {
                        var f = d.ue_err;
                        d.ue_url && !e && b && 0 < b.length && (e = new Image, a.iel.push(e), e.src = b, a.count && a.count("postbackImageSize", b.length));
                        if (t) {
                            var h = k.encodeURIComponent;
                            h && b && (e = new Image, b = "" + d.ue_fpf + h(b) + ":" + (+new z - d.ue_t0), a.iel.push(e), e.src = b)
                        } else a.log && (a.log(b, "uedata", {n: 1}), a.ielf.push(b));
                        f && !f.ts && f.startTimer();
                        a.b && (f = a.b, a.b = "", M(f, c, g, 1))
                    }
                }

                function v(b) {
                    var c = x ? x.type : A, d = c && 2 != c, e = a.bfini;
                    N || (e && 1 < e && (b += "&bfform=1", d || (a.isBFT = e - 1)), 2 == c && (b += "&bfnt=1", a.isBFT = a.isBFT || 1), a.ssw && a.isBFT && (q(a.isNRBF) && (c = a.ssw(a.oid), c.e || q(c.val) || (a.isNRBF = 1 < c.val ? 0 : 1)), q(a.isNRBF) || (b += "&nrbf=" + a.isNRBF)), a.isBFT && !a.isNRBF && (b += "&bft=" + a.isBFT));
                    return b
                }

                if (!a.paused && (b || q(c))) {
                    for (var m in c) c[r](m) && h(m, b, c[m]);
                    u("pc", b, c);
                    m = h("id", b) || a.id;
                    var g = a.url + "?" + p + "&v=" + a.v + "&id=" + m, F = h("ctb", b) || h("wb", b), l, s;
                    F && (g += "&ctb=" + F);
                    1 < d.ueinit && (g += "&ic=" + d.ueinit);
                    if (!("ld" != p && "ul" != p || b && b != m)) {
                        if ("ld" == p) {
                            try {
                                k[G] && k[G].isUeh && (k[G] = null)
                            } catch (E) {
                            }
                            if (k.chrome) for (s = 0; s < H.length; s++) Q(B, H[s]);
                            (s = J.ue_backdetect) && s.ue_back && s.ue_back.value++;
                            d._uess && (l = d._uess());
                            a.isl = 1
                        }
                        a._bf && (g += "&bf=" + a._bf());
                        d.ue_navtiming && f && (h("ctb", m, "1"), u("tc", A, A, I));
                        y && !R && (f && D(a.t, {
                            na_: f.navigationStart,
                            ul_: f.unloadEventStart,
                            _ul: f.unloadEventEnd,
                            rd_: f.redirectStart,
                            _rd: f.redirectEnd,
                            fe_: f.fetchStart,
                            lk_: f.domainLookupStart,
                            _lk: f.domainLookupEnd,
                            co_: f.connectStart,
                            _co: f.connectEnd,
                            sc_: f.secureConnectionStart,
                            rq_: f.requestStart,
                            rs_: f.responseStart,
                            _rs: f.responseEnd,
                            dl_: f.domLoading,
                            di_: f.domInteractive,
                            de_: f.domContentLoadedEventStart,
                            _de: f.domContentLoadedEventEnd,
                            _dc: f.domComplete,
                            ld_: f.loadEventStart,
                            _ld: f.loadEventEnd,
                            ntd: ("function" !== typeof y.now || q(I) ? 0 : new z(I + y.now()) - new z) + a.t0
                        }), x && D(a.t, {ty: x.type + a.t0, rc: x.redirectCount + a.t0}), R = 1);
                        D(a.t, {hob: d.ue_hob, hoe: d.ue_hoe});
                        a.ifr && (g += "&ifr=1")
                    }
                    u(p, b, c, e);
                    c = "ld" == p && b && h("wb", b);
                    var n;
                    c || b && b !== m || Z(b);
                    c || m == a.oid || $((h("t", b) || {}).tc || +h("t0", b), +h("t0", b));
                    (e = d.ue_mbl) && e.cnt && !c && (g += e.cnt());
                    c ? h("wb", b, 2) : "ld" == p && (a.lid = C(m));
                    for (n in a.sc) if (1 == h("wb", n)) break;
                    if (c) {
                        if (a.s) return;
                        g = w(g, null)
                    } else e = w(g, null), e != g && (e = v(e), a.b = e), l && (g += l), g = w(g, b || a.id);
                    g = v(g);
                    if (a.b || c) for (n in a.sc) 2 == h("wb", n) && delete a.sc[n];
                    l = 0;
                    a._rt && (g += "&rt=" + a._rt());
                    c || (a.s = 0, (l = d.ue_err) && 0 < l.ec && l.pec < l.ec && (l.pec = l.ec, g += "&ec=" + l.ec + "&ecf=" + l.ecf), l = h("ctb", b), h("t", b, {}));
                    a.tag && a.tag().length && (g += "&csmtags=" + a.tag().join("|"), a.tag = L());
                    n = a.viz || [];
                    (e = n.length) && (g += "&viz=" + n.splice(0, e).join("|"));
                    q(d.ue_pty) || (g += "&pty=" + d.ue_pty + "&spty=" + d.ue_spty + "&pti=" + d.ue_pti);
                    a.tabid && (g += "&tid=" + a.tabid);
                    a.aftb && (g += "&aftb=1");
                    !a._ui || b && b != m || (g += a._ui());
                    a.a = g;
                    M(g, p, l, c)
                }
            }

            function Z(a) {
                var b = k.ue_csm_markers || {}, c;
                for (c in b) b[r](c) && u(c, a, A, b[c])
            }

            function v(a, b, c) {
                c = c || k;
                if (c[S]) c[S](a, b, !1); else if (c[T]) c[T]("on" + a, b)
            }

            function Q(a, b, c) {
                c = c || k;
                if (c[U]) c[U](a, b, !1); else if (c[V]) c[V]("on" + a, b)
            }

            function W() {
                function a() {
                    d.onUl()
                }

                function b(a) {
                    return function () {
                        c[a] || (c[a] = 1, P(a))
                    }
                }

                var c = {}, e, f;
                d.onLd = b("ld");
                d.onLdEnd = b("ld");
                d.onUl = b("ul");
                e = {stop: b("os")};
                k.chrome ? (v(B, a), H.push(a)) : e[B] = d.onUl;
                for (f in e) e[r](f) && O(0, k, f, e[f]);
                d.ue_viz && ue_viz();
                v("load", d.onLd);
                u("ue")
            }

            function $(f, b) {
                var c = d.ue_mbl;
                c && c.ajax && c.ajax(f, b);
                a.tag("ajax-transition")
            }

            d.ueinit = (d.ueinit || 0) + 1;
            var a = d.ue = d.ue || {};
            a.t0 = k.aPageStart || d.ue_t0;
            a.id = d.ue_id;
            a.url = d.ue_url;
            a.rid = d.ue_id;
            a.a = "";
            a.b = "";
            a.h = {};
            a.s = 1;
            a.t = {};
            a.sc = {};
            a.iel = [];
            a.ielf = [];
            a.viz = [];
            a.v = "0.205540.0";
            a.paused = !1;
            var r = "hasOwnProperty", B = "beforeunload", G = "on" + B, S = "addEventListener",
                U = "removeEventListener", T = "attachEvent", V = "detachEvent", z = k.Date,
                y = k.performance || k.webkitPerformance, f = (y || {}).timing, x = (y || {}).navigation,
                I = (f || {}).navigationStart, t = d.ue_fpf, N = 0, R = 0, H = [], A;
            a.oid = C(a.id);
            a.lid = C(a.id);
            a._t0 = a.t0;
            a.tag = L();
            a.ifr = k.top !== k.self || k.frameElement ? 1 : 0;
            a.attach = v;
            a.detach = Q;
            if ("000-0000000-8675309" === d.ue_sid) {
                var X = E("cdn-rid"), Y = E("session-id");
                X && Y && K(X, Y)
            }
            d.uei = W;
            d.ueh = O;
            d.ues = h;
            d.uet = u;
            d.uex = P;
            a.reset = K;
            a.pause = function (d) {
                a.paused = d
            };
            W()
        })(ue_csm, window, ue_csm.document);
        ue.stub(ue, "event");
        ue.stub(ue, "onSushiUnload");
        ue.stub(ue, "onSushiFlush");
        ue.stub(ue, "log");
        ue.stub(ue, "onunload");
        ue.stub(ue, "onflush");
        (function (c) {
            var a = c.ue;
            a.cv = {};
            a.cv.scopes = {};
            a.count = function (d, c, b) {
                var e = {}, f = a.cv, g = b && 0 === b.c;
                e.counter = d;
                e.value = c;
                e.t = a.d();
                b && b.scope && (f = a.cv.scopes[b.scope] = a.cv.scopes[b.scope] || {}, e.scope = b.scope);
                if (void 0 === c) return f[d];
                f[d] = c;
                d = 0;
                b && b.bf && (d = 1);
                ue_csm.ue_sclog || !a.clog || 0 !== d || g ? a.log && a.log(e, "csmcount", {
                    c: 1,
                    bf: d
                }) : a.clog(e, "csmcount", {bf: d})
            };
            a.count("baselineCounter2", 1);
            a && a.event && (a.event({
                requestId: c.ue_id || "rid",
                server: c.ue_sn || "sn",
                obfuscatedMarketplaceId: c.ue_mid || "mid"
            }, "csm", "csm.CSMBaselineEvent.4"), a.count("nexusBaselineCounter", 1, {bf: 1}))
        })(ue_csm);
        var ue_hoe = +new Date();
    }
    window.ueinit = window.ue_ihb;</script> <!-- c1nq1f1hgw5tq21v6yvuzel8sfw3n3y2gcraot15fcj -->
<!-- sp:feature:aui-assets -->
    <link rel="stylesheet"
                                     href="https://images-eu.ssl-images-amazon.com/images/I/51tax7M48-L._RC|516fcOUE-HL.css,01evdoiemkL.css,01K+Ps1DeEL.css,31pdJv9iSzL.css,01VszOUTO6L.css,11UGC+GXOPL.css,21LK7jaicML.css,11L58Qpo0GL.css,21kyTi1FabL.css,01ruG+gDPFL.css,01YhS3Cs-hL.css,21GwE3cR-yL.css,019SHZnt8RL.css,01wAWQRgXzL.css,21bWcRJYNIL.css,11WgRxUdJRL.css,01dU8+SPlFL.css,11ocrgKoE-L.css,01SHjPML6tL.css,111-D2qRjiL.css,01QrWuRrZ-L.css,310Imb6LqFL.css,11Z1a0FxSIL.css,01cbS3UK11L.css,21mOLw+nYYL.css,01L8Y-JFEhL.css_.css?AUIClients/AmazonUI#not-trident.218320-T1"/>
    <script> (function (f, h, R, A) {
    function G(a) {
        x && x.tag && x.tag(q(":", "aui", a))
    }

    function v(a, b) {
        x && x.count && x.count("aui:" + a, 0 === b ? 0 : b || (x.count("aui:" + a) || 0) + 1)
    }

    function p(a) {
        try {
            return a.test(navigator.userAgent)
        } catch (b) {
            return !1
        }
    }

    function y(a, b, c) {
        a.addEventListener ? a.addEventListener(b, c, !1) : a.attachEvent && a.attachEvent("on" + b, c)
    }

    function q(a, b, c, e) {
        b = b && c ? b + a + c : b || c;
        return e ? q(a, b, e) : b
    }

    function H(a, b, c) {
        try {
            Object.defineProperty(a, b, {value: c, writable: !1})
        } catch (e) {
            a[b] = c
        }
        return c
    }

    function ua(a, b) {
        var c = a.length, e = c, g = function () {
            e-- || (S.push(b), T || (setTimeout(U, 0), T = !0))
        };
        for (g(); c--;) da[a[c]] ? g() : (B[a[c]] = B[a[c]] || []).push(g)
    }

    function va(a, b, c, e, g) {
        var d = h.createElement(a ? "script" : "link");
        y(d, "error", e);
        g && y(d, "load", g);
        a ? (d.type = "text/javascript", d.async = !0, c && /AUIClients|images[/]I/.test(b) && d.setAttribute("crossorigin", "anonymous"), d.src = b) : (d.rel = "stylesheet", d.href = b);
        h.getElementsByTagName("head")[0].appendChild(d)
    }

    function ea(a, b) {
        function c(c, e) {
            function g() {
                va(b, c, h, function (b) {
                    !I && h ? (h = !1, v("resource_retry"), g()) : (v("resource_error"), a.log("Asset failed to load: " + c, I ? "WARN" : A));
                    b && b.stopPropagation ? b.stopPropagation() : f.event && (f.event.cancelBubble = !0)
                }, e)
            }

            if (fa[c]) return !1;
            fa[c] = !0;
            v("resource_count");
            var h = !0;
            return !g()
        }

        if (b) {
            var e = 0, g = 0;
            c.andConfirm = function (a, b) {
                return c(a, function () {
                    e++;
                    b && b.apply(this, arguments)
                })
            };
            c.confirm = function () {
                g++
            };
            c.getCsriCounters = function () {
                return {reqs: e, full: g}
            }
        }
        return c
    }

    function wa(a, b, c) {
        for (var e = {
            name: a, guard: function (c) {
                return b.guardFatal(a, c)
            }, logError: function (c, d, e) {
                b.logError(c, d, e, a)
            }
        }, g = [], d = 0; d < c.length; d++) J.hasOwnProperty(c[d]) && (g[d] = V.hasOwnProperty(c[d]) ? V[c[d]](J[c[d]], e) : J[c[d]]);
        return g
    }

    function C(a, b, c, e, g) {
        return function (d, h) {
            function n() {
                var a = null;
                e ? a = h : "function" === typeof h && (p.start = D(), a = h.apply(f, wa(d, k, l)), p.end = D());
                if (b) {
                    J[d] = a;
                    a = d;
                    for (da[a] = !0; (B[a] || []).length;) B[a].shift()();
                    delete B[a]
                }
                p.done = !0
            }

            var k = g || this;
            "function" === typeof d && (h = d, d = A);
            b && (d = d ? d.replace(ga, "") : "__NONAME__", W.hasOwnProperty(d) && k.error(q(", reregistered by ", q(" by ", d + " already registered", W[d]), k.attribution), d), W[d] = k.attribution);
            for (var l = [], m = 0; m < a.length; m++) l[m] = a[m].replace(ga, "");
            var p = ha[d || "anon" + ++xa] = {depend: l, registered: D(), namespace: k.namespace};
            c ? n() : ua(l, k.guardFatal(d, n));
            return {
                decorate: function (a) {
                    V[d] = k.guardFatal(d, a)
                }
            }
        }
    }

    function ia(a) {
        return function () {
            var b = Array.prototype.slice.call(arguments);
            return {execute: C(b, !1, a, !1, this), register: C(b, !0, a, !1, this)}
        }
    }

    function X(a, b) {
        return function (c, e) {
            e || (e = c, c = A);
            var g = this.attribution;
            return function () {
                z.push(b || {attribution: g, name: c, logLevel: a});
                var d = e.apply(this, arguments);
                z.pop();
                return d
            }
        }
    }

    function K(a, b) {
        this.load = {js: ea(this, !0), css: ea(this)};
        H(this, "namespace", b);
        H(this, "attribution", a)
    }

    function ja() {
        h.body ? r.trigger("a-bodyBegin") : setTimeout(ja, 20)
    }

    function E(a, b) {
        a.className = Y(a, b) + " " + b
    }

    function Y(a, b) {
        return (" " + a.className + " ").split(" " + b + " ").join(" ").replace(/^ | $/g, "")
    }

    function ka(a) {
        try {
            return a()
        } catch (b) {
            return !1
        }
    }

    function L() {
        if (M) {
            var a = {w: f.innerWidth || n.clientWidth, h: f.innerHeight || n.clientHeight};
            5 < Math.abs(a.w - Z.w) || 50 < a.h - Z.h ? (Z = a, N = 4, (a = k.mobile || k.tablet ? 450 < a.w && a.w > a.h : 1250 <= a.w) ? E(n, "a-ws") : n.className = Y(n, "a-ws")) : 0 < N && (N--, la = setTimeout(L, 16))
        }
    }

    function ya(a) {
        (M = a === A ? !M : !!a) && L()
    }

    function za() {
        return M
    }

    function u(a, b) {
        return "sw:" + (b || "") + ":" + a + ":"
    }

    function ma() {
        na.forEach(function (a) {
            G(a)
        })
    }

    function t(a) {
        na.push(a)
    }

    function oa(a, b, c, e) {
        if (c) {
            b = p(/Chrome/i) && !p(/Edge/i) && !p(/OPR/i) && !a.capabilities.isAmazonApp && !p(new RegExp(aa + "bwv" + aa + "b"));
            var g = u(e, "browser"), d = u(e, "prod_mshop"), f = u(e, "beta_mshop");
            !a.capabilities.isAmazonApp && c.browser && b && (t(g + "supported"), c.browser.action(g, e));
            !b && c.browser && t(g + "unsupported");
            c.prodMshop && t(d + "unsupported");
            c.betaMshop && t(f + "unsupported")
        }
    }

    "use strict";
    var O = R.now = R.now || function () {
        return +new R
    }, D = function (a) {
        return a && a.now ? a.now.bind(a) : O
    }(f.performance), P = D(), l = f.AmazonUIPageJS || f.P;
    if (l && l.when && l.register) {
        for (var P = [], m = h.currentScript; m; m = m.parentElement) m.id && P.push(m.id);
        return l.log("A copy of P has already been loaded on this page.", "FATAL", P.join(" "))
    }
    var x = f.ue;
    G();
    G("aui_build_date:3.19.7-2019-10-21");
    var S = [], T = !1, U;
    U = function () {
        for (var a = setTimeout(U, 0), b = O(); S.length;) if (S.shift()(), 50 < O() - b) return;
        clearTimeout(a);
        T = !1
    };
    var da = {}, B = {}, fa = {}, I = !1;
    y(f, "beforeunload", function () {
        I = !0;
        setTimeout(function () {
            I = !1
        }, 1E4)
    });
    var ga = /^prv:/, W = {}, J = {}, V = {}, ha = {}, xa = 0, aa = String.fromCharCode(92), F, z = [], pa = f.onerror;
    f.onerror = function (a, b, c, e, g) {
        g && "object" === typeof g || (g = Error(a, b, c), g.columnNumber = e, g.stack = b || c || e ? q(aa, g.message, "at " + q(":", b, c, e)) : A);
        var d = z.pop() || {};
        g.attribution = q(":", g.attribution || d.attribution, d.name);
        g.logLevel = d.logLevel;
        g.attribution && console && console.log && console.log([g.logLevel || "ERROR", a, "thrown by", g.attribution].join(" "));
        z = [];
        pa && (d = [].slice.call(arguments), d[4] = g, pa.apply(f, d))
    };
    K.prototype = {
        logError: function (a, b, c, e) {
            b = {message: b, logLevel: c || "ERROR", attribution: q(":", this.attribution, e)};
            if (f.ueLogError) return f.ueLogError(a || b, a ? b : null), !0;
            console && console.error && (console.log(b), console.error(a));
            return !1
        },
        error: function (a, b, c, e) {
            a = Error(q(":", e, a, c));
            a.attribution = q(":", this.attribution, b);
            throw a;
        },
        guardError: X(),
        guardFatal: X("FATAL"),
        guardCurrent: function (a) {
            var b = z[z.length - 1];
            return b ? X(b.logLevel, b).call(this, a) : a
        },
        log: function (a, b, c) {
            return this.logError(null, a, b, c)
        },
        declare: C([], !0, !0, !0),
        register: C([], !0),
        execute: C([]),
        AUI_BUILD_DATE: "3.19.7-2019-10-21",
        when: ia(),
        now: ia(!0),
        trigger: function (a, b, c) {
            var e = O();
            this.declare(a, {data: b, pageElapsedTime: e - (f.aPageStart || NaN), triggerTime: e});
            c && c.instrument && F.when("prv:a-logTrigger").execute(function (b) {
                b(a)
            })
        },
        handleTriggers: function () {
            this.log("handleTriggers deprecated")
        },
        attributeErrors: function (a) {
            return new K(a)
        },
        _namespace: function (a, b) {
            return new K(a, b)
        }
    };
    var r = H(f, "AmazonUIPageJS", new K);
    F = r._namespace("PageJS", "AmazonUI");
    F.declare("prv:p-debug", ha);
    r.declare("p-recorder-events", []);
    r.declare("p-recorder-stop", function () {
    });
    H(f, "P", r);
    ja();
    if (h.addEventListener) {
        var qa;
        h.addEventListener("DOMContentLoaded", qa = function () {
            r.trigger("a-domready");
            h.removeEventListener("DOMContentLoaded", qa, !1)
        }, !1)
    }
    var n = h.documentElement, ba = function () {
        var a = ["O", "ms", "Moz", "Webkit"], b = h.createElement("div");
        return {
            testGradients: function () {
                b.style.cssText = "background-image:-webkit-gradient(linear,left top,right bottom,from(#1E4),to(white));background-image:-webkit-linear-gradient(left top,#1E4,white);background-image:linear-gradient(left top,#1E4,white);";
                return ~b.style.backgroundImage.indexOf("gradient")
            }, test: function (c) {
                var e = c.charAt(0).toUpperCase() + c.substr(1);
                c = (a.join(e + " ") + e + " " + c).split(" ");
                for (e = c.length; e--;) if ("" === b.style[c[e]]) return !0;
                return !1
            }, testTransform3d: function () {
                var a = !1;
                f.matchMedia && (a = f.matchMedia("(-webkit-transform-3d)").matches);
                return a
            }
        }
    }(), l = n.className, ra = /(^| )a-mobile( |$)/.test(l), sa = /(^| )a-tablet( |$)/.test(l), k = {
        audio: function () {
            return !!h.createElement("audio").canPlayType
        }, video: function () {
            return !!h.createElement("video").canPlayType
        }, canvas: function () {
            return !!h.createElement("canvas").getContext
        }, svg: function () {
            return !!h.createElementNS && !!h.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect
        }, offline: function () {
            return navigator.hasOwnProperty && navigator.hasOwnProperty("onLine") && navigator.onLine
        }, dragDrop: function () {
            return "draggable" in h.createElement("span")
        }, geolocation: function () {
            return !!navigator.geolocation
        }, history: function () {
            return !(!f.history || !f.history.pushState)
        }, webworker: function () {
            return !!f.Worker
        }, autofocus: function () {
            return "autofocus" in h.createElement("input")
        }, inputPlaceholder: function () {
            return "placeholder" in h.createElement("input")
        }, textareaPlaceholder: function () {
            return "placeholder" in h.createElement("textarea")
        }, localStorage: function () {
            return "localStorage" in f && null !== f.localStorage
        }, orientation: function () {
            return "orientation" in f
        }, touch: function () {
            return "ontouchend" in h
        }, gradients: function () {
            return ba.testGradients()
        }, hires: function () {
            var a = f.devicePixelRatio && 1.5 <= f.devicePixelRatio || f.matchMedia && f.matchMedia("(min-resolution:144dpi)").matches;
            v("hiRes" + (ra ? "Mobile" : sa ? "Tablet" : "Desktop"), a ? 1 : 0);
            return a
        }, transform3d: function () {
            return ba.testTransform3d()
        }, touchScrolling: function () {
            return p(/Windowshop|android|OS ([5-9]|[1-9][0-9]+)(_[0-9]{1,2})+ like Mac OS X|Chrome|Silk|Firefox|Trident.+?; Touch/i)
        }, ios: function () {
            return p(/OS [1-9][0-9]*(_[0-9]*)+ like Mac OS X/i) && !p(/trident|Edge/i)
        }, android: function () {
            return p(/android.([1-9]|[L-Z])/i) && !p(/trident|Edge/i)
        }, mobile: function () {
            return ra
        }, tablet: function () {
            return sa
        }, rtl: function () {
            return "rtl" === n.dir
        }
    };
    for (m in k) k.hasOwnProperty(m) && (k[m] = ka(k[m]));
    for (var ca = "textShadow textStroke boxShadow borderRadius borderImage opacity transform transition".split(" "), Q = 0; Q < ca.length; Q++) k[ca[Q]] = ka(function () {
        return ba.test(ca[Q])
    });
    var M = !0, la = 0, Z = {w: 0, h: 0}, N = 4;
    L();
    y(f, "resize", function () {
        clearTimeout(la);
        N = 4;
        L()
    });
    var ta = {
        getItem: function (a) {
            try {
                return f.localStorage.getItem(a)
            } catch (b) {
            }
        }, setItem: function (a, b) {
            try {
                return f.localStorage.setItem(a, b)
            } catch (c) {
            }
        }
    };
    n.className = Y(n, "a-no-js");
    E(n, "a-js");
    !p(/OS [1-8](_[0-9]*)+ like Mac OS X/i) || f.navigator.standalone || p(/safari/i) || E(n, "a-ember");
    l = [];
    for (m in k) k.hasOwnProperty(m) && k[m] && l.push("a-" + m.replace(/([A-Z])/g, function (a) {
        return "-" + a.toLowerCase()
    }));
    E(n, l.join(" "));
    n.setAttribute("data-aui-build-date", "3.19.7-2019-10-21");
    r.register("p-detect", function () {
        return {
            capabilities: k,
            localStorage: k.localStorage && ta,
            toggleResponsiveGrid: ya,
            responsiveGridEnabled: za
        }
    });
    p(/UCBrowser/i) || k.localStorage && E(n, ta.getItem("a-font-class"));
    r.declare("a-event-revised-handling", !1);
    var w;
    try {
        w = navigator.serviceWorker
    } catch (a) {
        G("sw:nav_err")
    }
    w && (y(w, "message", function (a) {
        a && a.data && v(a.data.k, a.data.v)
    }), w.controller && w.controller.postMessage("MSG-RDY"));
    var na = [], l = {reg: {}, unreg: {}};
    l.unreg.browser = {
        action: function (a, b) {
            var c = w.getRegistrations();
            c && c.then(function (c) {
                c.forEach(function (c) {
                    c.unregister().then(function () {
                        v(a + "success")
                    }).catch(function (c) {
                        r.logError(c, "[AUI SW] Failed to " + b + " service worker: ");
                        v(a + "failure")
                    })
                })
            })
        }
    };
    (function (a) {
        var b = a.reg, c = a.unreg;
        w && w.getRegistrations ? (F.when("A", "a-util").execute(function (a, b) {
            oa(a, b, c, "unregister")
        }), y(f, "load", function () {
            F.when("A", "a-util").execute(function (a, c) {
                oa(a, c, b, "register");
                ma()
            })
        })) : (b && (b.browser && t(u("register", "browser") + "unsupported"), b.prodMshop && t(u("register", "prod_mshop") + "unsupported"), b.betaMshop && t(u("register", "beta_mshop") + "unsupported")), c && (c.browser && t(u("unregister", "browser") + "unsupported"), c.prodMshop && t(u("unregister", "prod_mshop") + "unsupported"), c.betaMshop && t(u("unregister", "beta_mshop") + "unsupported")), ma())
    })(l);
    r.declare("a-fix-event-off", !1);
    v("pagejs:pkgExecTime", D() - P)
})(window, document, Date);
(function (a) {
    "use strict";
    if (!a.mix_d) {
        var q = [], r = {}, t = !a.Promise;
        t && P.when("3p-promise").register("@p/promise-is-ready", function (e) {
            a.Promise = e
        });
        a.mix_d = function (e, d, k) {
            "string" !== typeof e && a.P.error("CardJS C001");
            if (!r[e]) {
                var c = e.split(":", 2), h = c.length, b = 1 === h, f = c[h - 1].replace(/@capability\//, "@c/"),
                    l = 0 === f.indexOf("@c/"), h = k ? d : q, u = k || d, g = b ? a.P : a.P._namespace(c[0]), m,
                    v = [], n = [];
                d = [];
                k = h.length;
                for (c = 0; c < k; c++) b = h[c], "module" !== b && "require" !== b || g.error("CardJS C002"), "exports" === b ? (b = "mix:" + f + "-exports", g.declare(b, m = {}), d.push(b)) : "@p/" === b.substr(0, 3) ? d.push(b.substr(3)) : "@c/" === b.substr(0, 3) ? (n.push(c), v.push(b)) : d.push("mix:" + b);
                var p = 0;
                t && (d.unshift("@p/promise-is-ready"), p += 1);
                l && (d.unshift("mix:@amzn/mix.client-runtime"), p += 1);
                g.when.apply(g, d).register("mix:" + f, function () {
                    var b = q.slice.call(arguments, p);
                    if (l || 0 < n.length) {
                        var a = {
                            capabilities: v, cardModuleFactory: function (a) {
                                for (var d = b, e = a.length, c = 0; c < e; c++) d.splice(n[c], 0, a[c]);
                                a = u.apply(null, d);
                                a = m || a;
                                a.P = g;
                                return a
                            }
                        };
                        l && arguments[0].registerCapabilityModule(f, a);
                        return a
                    }
                    a = u.apply(null, b);
                    return m || a
                });
                g.when("mix:" + f).register("xcp:" + f, function (a) {
                    return a
                });
                r[e] = !0
            }
        };
        a.xcp_d = a.mix_d;
        P.when("mix:@amzn/mix.client-runtime").execute(function (a) {
            P.declare("xcp:@xcp/runtime", a)
        })
    }
})(window);
(window.AmazonUIPageJS ? AmazonUIPageJS : P).when('sp.load.js').execute(function () {
    (window.AmazonUIPageJS ? AmazonUIPageJS : P).load.js('https://images-eu.ssl-images-amazon.com/images/I/61-6nKPKyWL.js?AUIClients/AmazonUIjQuery');
    (window.AmazonUIPageJS ? AmazonUIPageJS : P).load.js('https://images-eu.ssl-images-amazon.com/images/I/11-BZEJ8lnL._RC|61GQ9IdK7HL.js,21Of0-9HPCL.js,012FVc3131L.js,119KAWlHU6L.js,51Vjz4D5exL.js,11AHlQhPRjL.js,016iHgpF74L.js,11aNYFFS5hL.js,116tgw9TSaL.js,211-p4GRUCL.js,01PoLXBDXWL.js,61kj4yH4b6L.js,11BOgvnnntL.js,31UWuPgtTtL.js,01rpauTep4L.js,01iyxuSGj4L.js,01p1Ax2BX5L.js_.js?AUIClients/AmazonUI#218320-T1.192338-T1');
    (window.AmazonUIPageJS ? AmazonUIPageJS : P).load.js('https://images-eu.ssl-images-amazon.com/images/I/41xnh5YdGuL.js?AUIClients/CardJsRuntimeBuzzCopyBuild');
}); </script> <!-- sp:feature:nav-inline-css --> <style>.nav-sprite-v1 .nav-sprite, .nav-sprite-v1 .nav-icon {
    background-image: url(https://images-eu.ssl-images-amazon.com/images/G/03/gno/sprites/nav-sprite-global_bluebeacon-1x_optimized_layout1._CB468502409_.png);
    background-position: 0 1000px;
    background-repeat: repeat-x;
}

.nav-spinner {
    background-image: url(https://images-eu.ssl-images-amazon.com/images/G/03/javascripts/lib/popover/images/snake._CB192194539_.gif);
    background-position: center center;
    background-repeat: no-repeat;
}

.nav-timeline-icon, .nav-access-image, .nav-timeline-prime-icon {
    background-image: url(https://images-eu.ssl-images-amazon.com/images/G/03/gno/sprites/timeline_sprite_1x._CB439968175_.png);
    background-repeat: no-repeat;
} </style>
    <!-- NAVYAAN CSS --> <link rel="stylesheet"
                               href="https://images-eu.ssl-images-amazon.com/images/I/21rQMjhzuzL._RC|719xYR-ku6L.css,11-cFHXC3yL.css,31DAr4NkZQL.css,21knqR7E30L.css,41tc24mJIGL.css,11G4HxMtMSL.css,31sgPDgo3OL.css,01XHMOHpK1L.css_.css?AUIClients/AmazonNavigationDesktopMetaAsset#desktop.language-de.de.222470-T1"/> <!-- sp:feature:host-assets --> <!doctype html>
    <html class="a-no-js" data-19ax5a9jf="dingo">
    <head>
        <script>var aPageStart = (new Date()).getTime();</script>
        <meta charset="utf-8"/>
        <title dir="ltr">Profil</title> <script
            type='text/javascript'>var ue_t0 = ue_t0 || +new Date();</script> <script
            type='text/javascript'> window.ue_ihb = (window.ue_ihb || window.ueinit || 0) + 1;
        if (window.ue_ihb === 1) {
            var ue_csm = window, ue_hob = +new Date();
            (function (d) {
                var e = d.ue = d.ue || {}, f = Date.now || function () {
                    return +new Date
                };
                e.d = function (b) {
                    return f() - (b ? 0 : d.ue_t0)
                };
                e.stub = function (b, a) {
                    if (!b[a]) {
                        var c = [];
                        b[a] = function () {
                            c.push([c.slice.call(arguments), e.d(), d.ue_id])
                        };
                        b[a].replay = function (b) {
                            for (var a; a = c.shift();) b(a[0], a[1], a[2])
                        };
                        b[a].isStub = 1
                    }
                };
                e.exec = function (b, a) {
                    return function () {
                        try {
                            return b.apply(this, arguments)
                        } catch (c) {
                            ueLogError(c, {attribution: a || "undefined", logLevel: "WARN"})
                        }
                    }
                }
            })(ue_csm);
            var ue_err_chan = 'jserr-rw';
            (function (d, e) {
                function h(f, b) {
                    if (!(a.ec > a.mxe) && f) {
                        a.ter.push(f);
                        b = b || {};
                        var c = f.logLevel || b.logLevel;
                        c && c !== k && c !== m && c !== n && c !== p || a.ec++;
                        c && c != k || a.ecf++;
                        b.pageURL = "" + (e.location ? e.location.href : "");
                        b.logLevel = c;
                        b.attribution = f.attribution || b.attribution;
                        a.erl.push({ex: f, info: b})
                    }
                }

                function l(a, b, c, e, g) {
                    d.ueLogError({
                        m: a,
                        f: b,
                        l: c,
                        c: "" + e,
                        err: g,
                        fromOnError: 1,
                        args: arguments
                    }, g ? {attribution: g.attribution, logLevel: g.logLevel} : void 0);
                    return !1
                }

                var k = "FATAL", m = "ERROR", n = "WARN", p = "DOWNGRADED", a = {
                    ec: 0, ecf: 0, pec: 0, ts: 0, erl: [], ter: [], mxe: 50, startTimer: function () {
                        a.ts++;
                        setInterval(function () {
                            d.ue && a.pec < a.ec && d.uex("at");
                            a.pec = a.ec
                        }, 1E4)
                    }
                };
                l.skipTrace = 1;
                h.skipTrace = 1;
                h.isStub = 1;
                d.ueLogError = h;
                d.ue_err = a;
                e.onerror = l
            })(ue_csm, window);
            var ue_id = '51BDYV5GTX3ZFDYJ0XZ7', ue_url = '/uedata/uedata', ue_navtiming = 1, ue_mid = 'A1PA6795UKMFR9',
                ue_sid = '262-7091204-2937867', ue_sn = 'www.amazon.de', ue_furl = 'fls-eu.amazon.de',
                ue_surl = 'https://unagi-eu.amazon.com/1/events/com.amazon.csm.nexusclient.prod', ue_int = 0,
                ue_csa_ajax = 0, ue_fcsn = 1, ue_urt = 3, ue_rpl_ns = 'cel-rpl', ue_ddq = 1,
                ue_fpf = '//fls-eu.amazon.de/1/batch/1/OP/A1PA6795UKMFR9:262-7091204-2937867:51BDYV5GTX3ZFDYJ0XZ7$uedata=s:',
                ue_sbuimp = 1, ue_swi = 1;

            function ue_viz() {
                (function (c, e, a) {
                    function k(b) {
                        if (c.ue.viz.length < p && !l) {
                            var a = b.type;
                            b = b.originalEvent;
                            /^focus./.test(a) && b && (b.toElement || b.fromElement || b.relatedTarget) || (a = e[m] || ("blur" == a || "focusout" == a ? "hidden" : "visible"), c.ue.viz.push(a + ":" + (+new Date - c.ue.t0)), "visible" == a && (ue.isl && uex("at"), l = 1))
                        }
                    }

                    for (var l = 0, f, g, m, n = ["", "webkit", "o", "ms", "moz"], d = 0, p = 20, h = 0; h < n.length && !d; h++) if (a = n[h], f = (a ? a + "H" : "h") + "idden", d = "boolean" == typeof e[f]) g = a + "visibilitychange", m = (a ? a + "V" : "v") + "isibilityState";
                    k({});
                    d && e.addEventListener(g, k, 0);
                    c.ue && d && (c.ue.pageViz = {event: g, propHid: f})
                })(ue_csm, document, window)
            };(function (d, k, K) {
                function D(a) {
                    return a && a.replace && a.replace(/^\s+|\s+$/g, "")
                }

                function r(a) {
                    return "undefined" === typeof a
                }

                function E(a, b) {
                    for (var c in b) b[t](c) && (a[c] = b[c])
                }

                function F(a) {
                    try {
                        var b = K.cookie.match(RegExp("(^| )" + a + "=([^;]+)"));
                        if (b) return b[2].trim()
                    } catch (c) {
                    }
                }

                function L(p, b) {
                    p && (d.ue_id = a.id = a.rid = p, v = v.replace(/((.*?:){2})(\w+)/, function (a, b) {
                        return b + p
                    }));
                    b && (v = v.replace(/(.*?:)(\w|-)+/, function (a, d) {
                        return d + b
                    }), d.ue_sid = b);
                    d.ue_fpf = v;
                    d.ue.tag("accelerated-experience")
                }

                function M() {
                    var a = {};
                    return function (b) {
                        b && (a[b] = 1);
                        b = [];
                        for (var c in a) a[t](c) && b.push(c);
                        return b
                    }
                }

                function w(d, b, c, e) {
                    e = e || +new A;
                    var f, m;
                    if (b || r(c)) {
                        if (d) for (m in f = b ? h("t", b) || h("t", b, {}) : a.t, f[d] = e, c) c[t](m) && h(m, b, c[m]);
                        return e
                    }
                }

                function h(d, b, c) {
                    var e = b && b != a.id ? a.sc[b] : a;
                    e || (e = a.sc[b] = {});
                    "id" === d && c && (N = 1);
                    return e[d] = c || e[d]
                }

                function O(d, b, c, e, f) {
                    c = "on" + c;
                    var h = b[c];
                    "function" === typeof h ? d && (a.h[d] = h) : h = function () {
                    };
                    b[c] = function (a) {
                        f ? (e(a), h(a)) : (h(a), e(a))
                    };
                    b[c] && (b[c].isUeh = 1)
                }

                function P(p, b, c, e) {
                    function q(b, c) {
                        var d = [b], g = 0, e = {}, f, k;
                        c ? (d.push("m=1"), e[c] = 1) : e = a.sc;
                        for (k in e) if (e[t](k)) {
                            var q = h("wb", k), m = h("t", k) || {}, n = h("t0", k) || a.t0, l;
                            if (c || 2 == q) {
                                q = q ? g++ : "";
                                d.push("sc" + q + "=" + k);
                                for (l in m) 3 >= l.length && !r(m[l]) && null !== m[l] && d.push(l + q + "=" + (m[l] - n));
                                d.push("t" + q + "=" + m[p]);
                                if (h("ctb", k) || h("wb", k)) f = 1
                            }
                        }
                        !G && f && d.push("ctb=1");
                        return d.join("&")
                    }

                    function m(b, c, g, e) {
                        if (b) {
                            var f = d.ue_err;
                            d.ue_url && !e && b && 0 < b.length && (e = new Image, a.iel.push(e), e.src = b, a.count && a.count("postbackImageSize", b.length));
                            if (v) {
                                var h = k.encodeURIComponent;
                                h && b && (e = new Image, b = "" + d.ue_fpf + h(b) + ":" + (+new A - d.ue_t0), a.iel.push(e), e.src = b)
                            } else a.log && (a.log(b, "uedata", {n: 1}), a.ielf.push(b));
                            f && !f.ts && f.startTimer();
                            a.b && (f = a.b, a.b = "", m(f, c, g, 1))
                        }
                    }

                    function x(b) {
                        var c = y ? y.type : B, d = c && 2 != c, e = a.bfini;
                        N || (e && 1 < e && (b += "&bfform=1", d || (a.isBFT = e - 1)), 2 == c && (b += "&bfnt=1", a.isBFT = a.isBFT || 1), a.ssw && a.isBFT && (r(a.isNRBF) && (c = a.ssw(a.oid), c.e || r(c.val) || (a.isNRBF = 1 < c.val ? 0 : 1)), r(a.isNRBF) || (b += "&nrbf=" + a.isNRBF)), a.isBFT && !a.isNRBF && (b += "&bft=" + a.isBFT));
                        return b
                    }

                    if (!a.paused && (b || r(c))) {
                        for (var l in c) c[t](l) && h(l, b, c[l]);
                        w("pc", b, c);
                        l = h("id", b) || a.id;
                        var g = a.url + "?" + p + "&v=" + a.v + "&id=" + l, G = h("ctb", b) || h("wb", b), n, u;
                        G && (g += "&ctb=" + G);
                        1 < d.ueinit && (g += "&ic=" + d.ueinit);
                        if (!("ld" != p && "ul" != p || b && b != l)) {
                            if ("ld" == p) {
                                try {
                                    k[H] && k[H].isUeh && (k[H] = null)
                                } catch (F) {
                                }
                                if (k.chrome) for (u = 0; u < I.length; u++) Q(C, I[u]);
                                (u = K.ue_backdetect) && u.ue_back && u.ue_back.value++;
                                d._uess && (n = d._uess());
                                a.isl = 1
                            }
                            a._bf && (g += "&bf=" + a._bf());
                            d.ue_navtiming && f && (h("ctb", l, "1"), w("tc", B, B, J));
                            z && !R && (f && E(a.t, {
                                na_: f.navigationStart,
                                ul_: f.unloadEventStart,
                                _ul: f.unloadEventEnd,
                                rd_: f.redirectStart,
                                _rd: f.redirectEnd,
                                fe_: f.fetchStart,
                                lk_: f.domainLookupStart,
                                _lk: f.domainLookupEnd,
                                co_: f.connectStart,
                                _co: f.connectEnd,
                                sc_: f.secureConnectionStart,
                                rq_: f.requestStart,
                                rs_: f.responseStart,
                                _rs: f.responseEnd,
                                dl_: f.domLoading,
                                di_: f.domInteractive,
                                de_: f.domContentLoadedEventStart,
                                _de: f.domContentLoadedEventEnd,
                                _dc: f.domComplete,
                                ld_: f.loadEventStart,
                                _ld: f.loadEventEnd,
                                ntd: ("function" !== typeof z.now || r(J) ? 0 : new A(J + z.now()) - new A) + a.t0
                            }), y && E(a.t, {ty: y.type + a.t0, rc: y.redirectCount + a.t0}), R = 1);
                            E(a.t, {hob: d.ue_hob, hoe: d.ue_hoe});
                            a.ifr && (g += "&ifr=1")
                        }
                        w(p, b, c, e);
                        c = "ld" == p && b && h("wb", b);
                        var s;
                        c || b && b !== l || Z(b);
                        c || l == a.oid || $(l, (h("t", b) || {}).tc || +h("t0", b), +h("t0", b));
                        (e = d.ue_mbl) && e.cnt && !c && (g += e.cnt());
                        c ? h("wb", b, 2) : "ld" == p && (a.lid = D(l));
                        for (s in a.sc) if (1 == h("wb", s)) break;
                        if (c) {
                            if (a.s) return;
                            g = q(g, null)
                        } else e = q(g, null), e != g && (e = x(e), a.b = e), n && (g += n), g = q(g, b || a.id);
                        g = x(g);
                        if (a.b || c) for (s in a.sc) 2 == h("wb", s) && delete a.sc[s];
                        n = 0;
                        a._rt && (g += "&rt=" + a._rt());
                        c || (a.s = 0, (n = d.ue_err) && 0 < n.ec && n.pec < n.ec && (n.pec = n.ec, g += "&ec=" + n.ec + "&ecf=" + n.ecf), n = h("ctb", b), h("t", b, {}));
                        a.tag && a.tag().length && (g += "&csmtags=" + a.tag().join("|"), a.tag = M());
                        s = a.viz || [];
                        (e = s.length) && (g += "&viz=" + s.splice(0, e).join("|"));
                        r(d.ue_pty) || (g += "&pty=" + d.ue_pty + "&spty=" + d.ue_spty + "&pti=" + d.ue_pti);
                        a.tabid && (g += "&tid=" + a.tabid);
                        a.aftb && (g += "&aftb=1");
                        !a._ui || b && b != l || (g += a._ui());
                        a.a = g;
                        m(g, p, n, c)
                    }
                }

                function Z(a) {
                    var b = k.ue_csm_markers || {}, c;
                    for (c in b) b[t](c) && w(c, a, B, b[c])
                }

                function x(a, b, c) {
                    c = c || k;
                    if (c[S]) c[S](a, b, !1); else if (c[T]) c[T]("on" + a, b)
                }

                function Q(a, b, c) {
                    c = c || k;
                    if (c[U]) c[U](a, b, !1); else if (c[V]) c[V]("on" + a, b)
                }

                function W() {
                    function a() {
                        d.onUl()
                    }

                    function b(a) {
                        return function () {
                            c[a] || (c[a] = 1, P(a))
                        }
                    }

                    var c = {}, e, f;
                    d.onLd = b("ld");
                    d.onLdEnd = b("ld");
                    d.onUl = b("ul");
                    e = {stop: b("os")};
                    k.chrome ? (x(C, a), I.push(a)) : e[C] = d.onUl;
                    for (f in e) e[t](f) && O(0, k, f, e[f]);
                    d.ue_viz && ue_viz();
                    x("load", d.onLd);
                    w("ue")
                }

                function $(f, b, c) {
                    var e = d.ue_mbl, h = k.csa, m = h && h("SPA"), h = h && h("Content");
                    e && e.ajax && e.ajax(b, c);
                    1 === k.ue_csa_ajax && m && h && (m("newPage", {
                        requestId: f,
                        transitionType: "soft"
                    }), h("get", "page")("emit", "loaded"));
                    a.tag("ajax-transition")
                }

                d.ueinit = (d.ueinit || 0) + 1;
                var a = d.ue = d.ue || {};
                a.t0 = k.aPageStart || d.ue_t0;
                a.id = d.ue_id;
                a.url = d.ue_url;
                a.rid = d.ue_id;
                a.a = "";
                a.b = "";
                a.h = {};
                a.s = 1;
                a.t = {};
                a.sc = {};
                a.iel = [];
                a.ielf = [];
                a.viz = [];
                a.v = "0.205676.0";
                a.paused = !1;
                var t = "hasOwnProperty", C = "beforeunload", H = "on" + C, S = "addEventListener",
                    U = "removeEventListener", T = "attachEvent", V = "detachEvent", A = k.Date,
                    z = k.performance || k.webkitPerformance, f = (z || {}).timing, y = (z || {}).navigation,
                    J = (f || {}).navigationStart, v = d.ue_fpf, N = 0, R = 0, I = [], B;
                a.oid = D(a.id);
                a.lid = D(a.id);
                a._t0 = a.t0;
                a.tag = M();
                a.ifr = k.top !== k.self || k.frameElement ? 1 : 0;
                a.attach = x;
                a.detach = Q;
                if ("000-0000000-8675309" === d.ue_sid) {
                    var X = F("cdn-rid"), Y = F("session-id");
                    X && Y && L(X, Y)
                }
                d.uei = W;
                d.ueh = O;
                d.ues = h;
                d.uet = w;
                d.uex = P;
                a.reset = L;
                a.pause = function (d) {
                    a.paused = d
                };
                W()
            })(ue_csm, window, ue_csm.document);
            ue.stub(ue, "log");
            ue.stub(ue, "onunload");
            ue.stub(ue, "onflush");
            (function (c) {
                var a = c.ue;
                a.cv = {};
                a.cv.scopes = {};
                a.count = function (d, c, b) {
                    var e = {}, f = a.cv, g = b && 0 === b.c;
                    e.counter = d;
                    e.value = c;
                    e.t = a.d();
                    b && b.scope && (f = a.cv.scopes[b.scope] = a.cv.scopes[b.scope] || {}, e.scope = b.scope);
                    if (void 0 === c) return f[d];
                    f[d] = c;
                    d = 0;
                    b && b.bf && (d = 1);
                    ue_csm.ue_sclog || !a.clog || 0 !== d || g ? a.log && a.log(e, "csmcount", {
                        c: 1,
                        bf: d
                    }) : a.clog(e, "csmcount", {bf: d})
                };
                a.count("baselineCounter2", 1);
                a && a.event && (a.event({
                    requestId: c.ue_id || "rid",
                    server: c.ue_sn || "sn",
                    obfuscatedMarketplaceId: c.ue_mid || "mid"
                }, "csm", "csm.CSMBaselineEvent.4"), a.count("nexusBaselineCounter", 1, {bf: 1}))
            })(ue_csm);
            var ue_hoe = +new Date();
        }
        window.ueinit = window.ue_ihb; </script> <!-- 1lxv4brp7w2x0612fdszhxya8h796 -->
        <link rel="stylesheet"
              href="https://images-eu.ssl-images-amazon.com/images/I/01LAv8I1AxL._RC|11BhDAo+YQL.css,01LKsGfpclL.css,01PTkp9JOCL.css,017IH9bX79L.css,11hlEWdpPvL.css,31dZWP+VLdL.css_.css?AUIClients/DesktopSRPMetaAsset"/> <link
            rel="stylesheet"
            href="https://images-eu.ssl-images-amazon.com/images/I/51tax7M48-L._RC|516fcOUE-HL.css,01evdoiemkL.css,01K+Ps1DeEL.css,31pdJv9iSzL.css,01VszOUTO6L.css,11UGC+GXOPL.css,21LK7jaicML.css,11L58Qpo0GL.css,21kyTi1FabL.css,01ruG+gDPFL.css,01YhS3Cs-hL.css,21GwE3cR-yL.css,019SHZnt8RL.css,01wAWQRgXzL.css,21bWcRJYNIL.css,11WgRxUdJRL.css,01dU8+SPlFL.css,11ocrgKoE-L.css,01SHjPML6tL.css,111-D2qRjiL.css,01QrWuRrZ-L.css,310Imb6LqFL.css,11Z1a0FxSIL.css,01cbS3UK11L.css,21mOLw+nYYL.css,01L8Y-JFEhL.css_.css?AUIClients/AmazonUI#not-trident.218320-T1"/> <script> (window.AmazonUIPageJS ? AmazonUIPageJS : P).load.js('https://images-eu.ssl-images-amazon.com/images/I/31KrXGaozUL._RC|31G9wRc+mNL.js,01-4MbsHMWL.js,11gFiYe7KbL.js,416F0-woR3L.js,31yCl0u2BnL.js,51EdMiQk+pL.js,41EGoU24hlL.js_.js?AUIClients/DesktopSRPMetaAsset#155248-T1.188847-T1');
    (window.AmazonUIPageJS ? AmazonUIPageJS : P).load.js('https://images-eu.ssl-images-amazon.com/images/I/61-6nKPKyWL._RC|11-BZEJ8lnL.js,61GQ9IdK7HL.js,21Of0-9HPCL.js,012FVc3131L.js,119KAWlHU6L.js,51Vjz4D5exL.js,11AHlQhPRjL.js,016iHgpF74L.js,11aNYFFS5hL.js,116tgw9TSaL.js,211-p4GRUCL.js,01PoLXBDXWL.js,61kj4yH4b6L.js,11BOgvnnntL.js,31UWuPgtTtL.js,01rpauTep4L.js,01iyxuSGj4L.js,01p1Ax2BX5L.js_.js?AUIClients/AmazonUI#218320-T1.192338-T1'); </script> <script
            type='text/javascript'> window.ue_ihe = (window.ue_ihe || 0) + 1;
        if (window.ue_ihe === 1) {
            (function (k, l, g) {
                function m(a) {
                    c || (c = b[a.type].id, "undefined" === typeof a.clientX ? (e = a.pageX, f = a.pageY) : (e = a.clientX, f = a.clientY), 2 != c || h && (h != e || n != f) ? (r(), d.isl && l.setTimeout(function () {
                        p("at", d.id)
                    }, 0)) : (h = e, n = f, c = 0))
                }

                function r() {
                    for (var a in b) b.hasOwnProperty(a) && d.detach(a, m, b[a].parent)
                }

                function s() {
                    for (var a in b) b.hasOwnProperty(a) && d.attach(a, m, b[a].parent)
                }

                function t() {
                    var a = "";
                    !q && c && (q = 1, a += "&ui=" + c);
                    return a
                }

                var d = k.ue, p = k.uex, q = 0, c = 0, h, n, e, f, b = {
                    click: {id: 1, parent: g},
                    mousemove: {id: 2, parent: g},
                    scroll: {id: 3, parent: l},
                    keydown: {id: 4, parent: g}
                };
                d && p && (s(), d._ui = t)
            })(ue_csm, window, document);
            (function (s, l) {
                function m(b, e, c) {
                    c = c || new Date(+new Date + t);
                    c = "expires=" + c.toUTCString();
                    n.cookie = b + "=" + e + ";" + c + ";path=/"
                }

                function p(b) {
                    b += "=";
                    for (var e = n.cookie.split(";"), c = 0; c < e.length; c++) {
                        for (var a = e[c]; " " == a.charAt(0);) a = a.substring(1);
                        if (0 === a.indexOf(b)) return decodeURIComponent(a.substring(b.length, a.length))
                    }
                    return ""
                }

                function q(b, e, c) {
                    if (!e) return b;
                    -1 < b.indexOf("{") && (b = "");
                    for (var a = b.split("&"), f, d = !1, h = !1, g = 0; g < a.length; g++) f = a[g].split(":"), f[0] == e ? (!c || d ? a.splice(g, 1) : (f[1] = c, a[g] = f.join(":")), h = d = !0) : 2 > f.length && (a.splice(g, 1), h = !0);
                    h && (b = a.join("&"));
                    !d && c && (0 < b.length && (b += "&"), b += e + ":" + c);
                    return b
                }

                var k = s.ue || {}, t = 6048E7, n = ue_csm.document || l.document, r = null, d;
                a:{
                    try {
                        d = l.localStorage;
                        break a
                    } catch (u) {
                    }
                    d = void 0
                }
                k.count && k.count("csm.cookieSize", document.cookie.length);
                k.cookie = {
                    get: p, set: m, updateCsmHit: function (b, e, c) {
                        try {
                            var a;
                            if (!(a = r)) {
                                var f;
                                a:{
                                    try {
                                        if (d && d.getItem) {
                                            f = d.getItem("csm-hit");
                                            break a
                                        }
                                    } catch (k) {
                                    }
                                    f = void 0
                                }
                                a = f || p("csm-hit") || "{}"
                            }
                            a = q(a, b, e);
                            r = a = q(a, "t", +new Date);
                            try {
                                d && d.setItem && d.setItem("csm-hit", a)
                            } catch (h) {
                            }
                            m("csm-hit", a, c)
                        } catch (g) {
                            "function" == typeof l.ueLogError && ueLogError(Error("Cookie manager: " + g.message), {logLevel: "WARN"})
                        }
                    }
                }
            })(ue_csm, window);
            (function (l, d) {
                function c(b) {
                    b = "";
                    var c = a.isBFT ? "b" : "s", d = "" + a.oid, f = "" + a.lid, g = d;
                    d != f && 20 == f.length && (c += "a", g += "-" + f);
                    a.tabid && (b = a.tabid + "+");
                    b += c + "-" + g;
                    b != e && 100 > b.length && (e = b, a.cookie ? a.cookie.updateCsmHit(m, b + ("|" + +new Date)) : document.cookie = "csm-hit=" + b + ("|" + +new Date) + n
                        <g:render template="profilePart1"/>
                        <g:render template="profilePart2"/>
                        <g:render template="profilePart3"/>
                        <g:render template="profilePart9"/>
                        <g:render template="profilePart10"/>

                        < /div>
                        < /body>
                        < /html>