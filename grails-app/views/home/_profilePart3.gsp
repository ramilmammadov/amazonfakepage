<div id="profile_v5">
    <div data-reactroot="" class="a-section profile-v5-desktop-background">
        <div class="a-section profile-v5-desktop">
            <div class="a-changeover a-changeover-manual profile-edited-toast" style="display: none;">
                <div class="a-changeover-inner">
                    <i class="a-icon a-icon-checkmark-inverse">
                    </i>
                    <strong class="a-size-medium">Gespeichert</strong>
                </div>
            </div>
            <!-- react-empty: 7 -->
            <div id="customer-profile-avatar-header" class="avatar-header-container">
                <div id="cover-photo-image" class="a-section a-spacing-none">
                    <div class="a-section a-spacing-none desktop cover-photo"
                         style="background-image: url(&quot;//d1k8kvpjaf8geh.cloudfront.net/gp/profile/assets/default_desktop_cover_photo_small-fa94c636796d18ebee73e32e4076d119a52366660d5660b5b2e49f62e036575a.png&quot;); background-size: contain;">
                        <img alt="" src="/avatar/cover/default/amzn1.account.AE6WN2URQIA5QGCHFZIIEGKEMJUQ"
                             class="cover-photo-with-cropping" id="cover-image-with-cropping">
                    </div>
                </div>

                <div id="customer-profile-avatar-image" class="a-section desktop avatar-image-container">
                    <div class="a-section updated-profile-image-holder desktop">
                        <div class="a-section a-spacing-none circular-avatar-image"
                             style="background-image: url(&quot;//d1k8kvpjaf8geh.cloudfront.net/gp/profile/assets/search_avatar-8059b2ed8a963eda51ee0b024a379bc98b88e8b72ba77c7c37204308ce09b47b.png&quot;); background-size: contain;">
                            <img alt=""
                                <g:if test="${profile?.image}">
                                    src="${createLink(controller: 'imageDisplayer', action: 'index', params: [photoName: profile?.image])}"
                                </g:if>
                                <g:else>
                                    src="https://images-eu.ssl-images-amazon.com/images/S/amazon-avatars-global/default._CR0,0,1024,1024_SX460_.png"
                                </g:else>
                                 id="avatar-image">
                        </div>
                    </div>
                </div>
            </div>

            <div class="desktop padded card name-header-card">
                <div class="a-row a-spacing-none desktop card-header">
                </div>

                <div class="a-row">
                    <div id="customer-profile-name-header" class="a-section desktop name-header-widget">
                        <div class="a-row header">
                        </div>

                        <div class="a-row a-spacing-none name-container">
                            <span class="a-size-extra-large">${profile?.name}</span>
                            <!-- react-empty: 89 -->
                        </div>

                        <div class="name-header-footer-placeholder">
                        </div>

                        <div class="a-row name-header-footer-container">
                        </div>
                    </div>
                </div>
            </div>

            <div class="a-section activity-area-container">
                <div class="deck-container sub" style="width: 320px;">
                    <div id="visitor-view-nudge"
                         class="a-section a-spacing-base a-padding-none desktop nudge-container">
                        <div class="a-box a-box-normal msg-banner">
                            <div class="a-box-inner">
                                <div class="a-section close-btn">
                                    <span id="visitor-view-nudge-nudge-message-dismiss">✕</span>
                                </div>

                                <div class="a-section a-spacing-top-base msg-banner-body">
                                    <div class="a-row content">
                                        <span class="a-size-base">Haben Sie Ihr Profil bereits angesehen? Stellen Sie sicher, dass es auf dem neuesten Stand ist!</span>
                                        <br>
                                        <a href="/gp/profile?ref=prof_v_vn">
                                            <span class="a-size-base">Ihr Profil anzeigen</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- react-empty: 90 -->
                    <div class="desktop padded card">
                        <div class="a-row a-spacing-none desktop card-header">
                            <span class="desktop card-title">Informationen</span>
                        </div>

                        <div class="a-row">
                            <div class="a-section">
                                <div class="a-fixed-right-grid">
                                    <div class="a-fixed-right-grid-inner" style="padding-right: 0px;">
                                    </div>
                                </div>

                                <div class="a-section a-spacing-top-base">
                                    <div class="a-row a-spacing-base">
                                        <div class="a-column a-span12">
                                            <span class="a-size-base">Rang des Rezensenten</span>

                                            <div class="a-row">
                                                <span class="a-size-base">#${profile?.ranking}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="a-section user-link-section">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- react-empty: 88 -->
                </div>

                <div class="deck-container main" style="width: 540px;">
                    <div class="desktop padded card dashboard-desktop-card">
                        <div class="a-row a-spacing-none desktop card-header">
                            <span class="desktop card-title">Überblick</span>
                        </div>

                        <div class="a-row">
                            <div class="dashboard-desktop-stats-section">
                                <div class="dashboard-desktop-stat large-margin-right">
                                    <a class="dashboard-desktop-stat-link">
                                        <div>
                                            <div class="dashboard-desktop-stat-value">
                                                <span class="a-size-large a-color-base">${helpfulVotes}</span>
                                            </div>

                                            <div class="dashboard-desktop-stat-type">
                                                <span class="a-size-small a-color-base">Nützliche Stimmen</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="dashboard-desktop-stat large-margin-right">
                                    <a class="dashboard-desktop-stat-link">
                                        <div>
                                            <div class="dashboard-desktop-stat-value">
                                                <span class="a-size-large a-color-base">${profile?.profileReviews?.size()}</span>
                                            </div>

                                            <div class="dashboard-desktop-stat-type">
                                                <span class="a-size-small a-color-base">Rezensionen</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="customer-profile-timeline" class="a-section">
                        <div id="profile-at-feed"
                             class="desktop padded card profile-at-header-card profile-at-header-card-desktop undefined">
                            <div class="a-row a-spacing-none desktop card-header">
                            </div>

                            <div class="a-row">
                                <div class="a-section a-spacing-none profile-at-header-title profile-at-header-title-desktop">
                                    <span class="a-size-medium">Community-Aktivitäten</span>
                                </div>

                                <div class="a-section profile-at-type-filter profile-at-type-filter-desktop">
                                    <span class="a-dropdown-container">
                                        <label class="a-native-dropdown">Ansicht:</label>
                                        <select name="activity-timeline-filter" autocomplete="off"
                                                data-a-touch-header="Activity Filter" tabindex="0"
                                                class="a-native-dropdown">
                                            <option class="a-prompt" value="">Alle Aktivitäten</option>
                                            <option value="all">Alle Aktivitäten</option>
                                        </select>
                                        <span tabindex="-1" id="customer-profile-filter"
                                              class="a-button a-button-dropdown" aria-hidden="true">
                                            <span class="a-button-inner">
                                                <span class="a-button-text a-declarative"
                                                      data-action="a-dropdown-button" role="button" aria-hidden="true">
                                                    <span class="a-dropdown-label">Ansicht:</span>
                                                    <span class="a-dropdown-prompt">Alle Aktivitäten</span>
                                                </span>
                                                <i class="a-icon a-icon-dropdown">
                                                </i>
                                            </span>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div id="profile-at-card-container" class="a-section">

                            <g:each in="${profile?.profileReviews?.sort { a, b -> b?.review?.id <=> a?.review?.id }}" var="profileReview">
                                <div class="desktop card profile-at-card profile-at-review-box">
                                    <div class="a-row a-spacing-none desktop card-header">
                                    </div>

                                    <div class="a-row">
                                        <div class="a-row a-spacing-none profile-at-header profile-at-desktop-header">
                                            <div class="a-row a-spacing-mini profile-at-user-info">
                                                <div class="a-profile" data-a-size="small" data-a-descriptor="true">
                                                    <div aria-hidden="true" class="a-profile-avatar-wrapper">
                                                        <div class="a-profile-avatar">
                                                            <img <g:if test="${profile?.image}">
                                                                src="${createLink(controller: 'imageDisplayer', action: 'index', params: [photoName: profile?.image])}"
                                                            </g:if>
                                                                <g:else>
                                                                    src="https://images-eu.ssl-images-amazon.com/images/S/amazon-avatars-global/default._CR0,0,1024,1024_SX460_.png"
                                                                </g:else>
                                                                    class=""
                                                                    data-src="/avatar/default/amzn1.account.AE6WN2URQIA5QGCHFZIIEGKEMJUQ?square=true&amp;max_width=460">
                                                            <noscript>
                                                                <img src="/avatar/default/amzn1.account.AE6WN2URQIA5QGCHFZIIEGKEMJUQ?square=true&amp;max_width=460">
                                                            </noscript>
                                                        </div>
                                                    </div>

                                                    <div class="a-profile-content">
                                                        <span class="a-profile-name">${profile?.name}</span>
                                                        <span class="a-profile-descriptor">Hat ein Produkt bewertet  · ${profileReview?.review?.date}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="a-divider-normal profile-at-divider">

                                        <div class="a-section profile-at-content">
                                            <a class="a-link-normal profile-at-review-link a-text-normal"
                                               href="${createLink(controller: 'home', action: 'info', id: profileReview?.review?.path)}">
                                                <div class="a-section a-spacing-mini">
                                                    <div class="a-row a-spacing-mini">
                                                        <i class="a-icon a-icon-star a-star-5 profile-at-review-stars">
                                                            <span class="a-icon-alt">5 von fünf Sternen</span>
                                                        </i>
                                                        <span class="a-size-small a-color-state profile-at-review-badge a-text-bold">Verifizierter kauf</span>
                                                    </div>
                                                </div>

                                                <div class="a-section a-spacing-none">
                                                    <h1 class="a-size-base a-spacing-none a-color-base profile-at-review-title a-text-bold">
                                                        <span>
                                                            <span>Ich rate</span>
                                                        </span>
                                                    </h1>

                                                    <p style="white-space: normal"
                                                       class="a-spacing-small a-spacing-top-mini a-color-base profile-at-review-text profile-at-review-text-desktop">
                                                        ${profileReview?.review?.reviewContent?.take(240)}
                                                     </p>
                                                </div>
                                            </a>

                                            <p class="a-spacing-medium">
                                                <a class="a-link-normal profile-at-review-link a-text-normal"
                                                   href="${createLink(controller: 'home', action: 'info', id: profileReview?.review?.path)}">Vollständige Rezension anzeigen</a>
                                            </p>

                                            <div class="a-row a-spacing-medium">
                                                <a class="a-link-normal profile-at-product-box-link a-text-normal"
                                                   href="${createLink(controller: 'home', action: 'info', id: profileReview?.review?.path)}">
                                                    <div class="a-section a-spacing-none profile-at-product-box">
                                                        <div class="a-section profile-at-product-image-container profile-at-product-box-element">
                                                            <img alt=""
                                                                 src="${createLink(controller: 'imageDisplayer', action: 'index', params: [photoName: profileReview?.review?.productImage])}"
                                                                 class="profile-at-product-image">
                                                        </div>

                                                        <div class="a-section profile-at-product-title-container profile-at-product-box-element">
                                                            <span>
                                                                ${profileReview?.review?.productTitle}
                                                            </span>

                                                            <div class="a-row profile-at-product-review-stars">
                                                                <i class="a-icon a-icon-star-mini a-star-mini-4">
                                                                    <span class="a-icon-alt">3.8 von fünf Sternen</span>
                                                                </i>
                                                                <span class="a-size-small a-color-tertiary profile-at-product-review-stars-count">61</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            <p class="a-spacing-medium">
                                                <span class="a-size-small a-color-tertiary profile-at-review-helpful-message-text">1 hilfreiche Stimmen</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </g:each>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>