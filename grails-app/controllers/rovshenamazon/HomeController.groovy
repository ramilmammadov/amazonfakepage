package rovshenamazon

class HomeController {
    def info() {
        def review = Review?.findByPath(params?.id)

        def starIconStarCount
        if (review.starCount.contains('.5')) {
            starIconStarCount = review.starCount.replace(".", "-")
        } else {
            starIconStarCount = Double.parseDouble(review.starCount).round().toString().replace(".", "-")
        }
        [review: review, starIconStarCount: starIconStarCount]
    }

    def profile() {
        def profile = Profile?.findByPath(params?.path)
        def helpfulVotes = profile?.profileReviews?.review?.helpfulVotes?.sum()
        [profile: profile, helpfulVotes: helpfulVotes]
    }
}
