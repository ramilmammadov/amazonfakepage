package rovshenamazon

import grails.util.Holders

class ImageDisplayerController {
    def cookieService
    def sourcePath = Holders.config.source.path

    def index() {
        def photoPath = sourcePath
        def photoName = params?.photoName

        String specificFolder
        File photo

        try {
            if (photoName.isEmpty() || photoName == null || photoName.equals("null")) {
                specificFolder = sourcePath + "/default"
                photo = new File(specificFolder + ".png")
                response.setHeader("Content-disposition", "inline")
            } else {
                specificFolder = photoPath
                photo = new File(specificFolder + File.separator + photoName)
                response.setHeader("Content-disposition", "inline")
            }
            if (photo.exists()) {
                response.setContentType("image/jpeg")
                response.outputStream << (photo.bytes)
                response.outputStream.flush()
                response.setHeader("Content-disposition", "inline")
            } else {
                specificFolder = photoPath
                photo = new File(photoPath + File.separator + photoName)
                response.setHeader("Content-disposition", "inline")
            }
        } catch (Exception e) {
            e.stackTrace
        }
        return
    }
}

