package rovshenamazon

class AdministratorController {
    def adminService

    def index() {
        def list = Review?.findAll([sort: 'id', order: 'desc', max: 1000])
        [list: list]
    }

    def add() {
        [review: Review?.get(params?.getLong('id'))]
    }

    def saveReview() {
        adminService.saveReview(params, request)
        redirect(action: 'index')
    }

    def delete() {
        adminService.delete(params)
        redirect(action: 'index')
    }

    def generalParams() {
        def configParams = ConfigParams?.findAll()
        [configParams: configParams]
    }

    def saveConfigParams() {
        adminService.saveConfigParams(params)
        redirect(action: 'generalParams')
    }

    def profileList() {
        def profiles = Profile?.findAll()
        [profiles: profiles]
    }

    def addProfile() {
        def profile = Profile?.get(params?.getLong('id'))
        [profile: profile]
    }

    def deleteProfile() {
        adminService.deleteProfile(params)
        redirect(action: 'profileList')
    }

    def saveProfile() {
        adminService.saveProfile(params, request)
        redirect(action: 'profileList')
    }
}
