package rovshenamazon

class Profile {
    String name
    String ranking
    String image
    String path
    static hasMany = [profileReviews: ProfileReview]
    static constraints = {
        image nullable: true, blank: true
    }
}
