package rovshenamazon

class Review {
    String title
    String title1
    String name
    String nameLink
    int startCount //starCount1
    String date
    String title2
    String color
    String colorLink
    String verified
    String reviewContent
    String image1
    String image2
    String image3
    String image4
    String image5
    String image6
    String image7
    String image8
    String productImage
    String productTitle
    String productTitleLink
    String byWhom
    String byWhomLink
    String starCount
    String customerRatingsText
    int star5Perc
    int star4Perc
    int star3Perc
    int star2Perc
    int star1Perc
    String price
    String shippingInfo
    String path
    Integer helpfulVotes
    static constraints = {
        title nullable: true, blank: true
        title1 nullable: true, blank: true
        name nullable: true, blank: true
        startCount nullable: true, blank: true
        title2 nullable: true, blank: true
        color nullable: true, blank: true
        verified nullable: true, blank: true
        reviewContent nullable: true, blank: true
        image1 nullable: true, blank: true
        image2 nullable: true, blank: true
        image3 nullable: true, blank: true
        image4 nullable: true, blank: true
        image5 nullable: true, blank: true
        image6 nullable: true, blank: true
        image7 nullable: true, blank: true
        image8 nullable: true, blank: true
        productImage nullable: true, blank: true
        productTitle nullable: true, blank: true
        byWhom nullable: true, blank: true
        starCount nullable: true, blank: true
        customerRatingsText nullable: true, blank: true
        star5Perc nullable: true, blank: true
        star4Perc nullable: true, blank: true
        star3Perc nullable: true, blank: true
        star2Perc nullable: true, blank: true
        star1Perc nullable: true, blank: true
        price nullable: true, blank: true
        shippingInfo nullable: true, blank: true
    }
}
