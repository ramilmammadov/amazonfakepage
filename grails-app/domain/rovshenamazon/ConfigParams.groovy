package rovshenamazon

class ConfigParams {
    String k
    String v
    static constraints = {
        v nullable: true, blank: true
    }
}
