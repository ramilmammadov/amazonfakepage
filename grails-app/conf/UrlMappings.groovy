class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller: 'home',action: 'index')
		"/RBBGM4842TXQ2/ref=pe_1604851_66412761_cm_rv_eml_rv0_rv/${id}"(controller: 'home',action: 'info')
		"/profile/amzn1.account.AE6WN2URQIA5QGCHFZIIEGKEMJUQ/${path}"(controller: 'home',action: 'profile')
		"500"(view:'/error')
		"404"(view:'/error')
	}
}
