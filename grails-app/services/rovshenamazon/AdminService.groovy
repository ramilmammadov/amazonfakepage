package rovshenamazon

import grails.util.Holders
import org.h2.util.Profiler

class AdminService {
    def sourcePath = Holders.config.source.path
    def transactional = false

    def saveReview(params, request) {
        def review = params?.getLong('id') ? Review?.get(params?.getLong('id')) : new Review()

        review?.title = params?.title
        review?.title1 = params?.title1
        review?.name = params?.name
        review?.path = params?.path
        review?.nameLink = params?.nameLink
        review?.startCount = params?.getInt('startCount')
        review?.date = params?.date
        review?.title2 = params?.title2
        review?.color = params?.color
        review?.colorLink = params?.colorLink
        review?.verified = params?.verified
        review?.reviewContent = params?.reviewContent
        review?.productTitle = params?.productTitle
        review?.productTitleLink = params?.productTitleLink
        review?.byWhom = params?.byWhom
        review?.byWhomLink = params?.byWhomLink
        review?.starCount = params?.starCount
        review?.customerRatingsText = params?.customerRatingsText
        review?.star5Perc = params?.getInt('star5Perc')
        review?.star4Perc = params?.getInt('star4Perc')
        review?.star3Perc = params?.getInt('star3Perc')
        review?.star2Perc = params?.getInt('star2Perc')
        review?.star1Perc = params?.getInt('star1Perc')
        review?.price = params?.price
        review?.helpfulVotes = params?.getInt('helpfulVotes')
        review?.shippingInfo = params?.shippingInfo

        int counter = 0
        def files = request?.getFiles("reviewImages")
        if (files) {
            for (file in files) {
                counter++
                if (file && !file.empty) {
                    def matcher = (file.getOriginalFilename() =~ /.*\.(.*)$/)
                    def extension
                    if (matcher.matches()) {
                        extension = String?.valueOf(matcher[0][1])?.toLowerCase()
                    }
                    if (extension == 'jpeg' || extension == 'png' || extension == 'bmp' || extension == 'jpg') {
                        String fileName = UUID.randomUUID().toString() + "." + extension
                        String dir = sourcePath
                        def filePath = new File(dir)
                        filePath.mkdirs()
                        if (counter == 1) {
                            review?.image1 = fileName
                        }
                        if (counter == 2) {
                            review?.image2 = fileName
                        }
                        if (counter == 3) {
                            review?.image3 = fileName
                        }
                        if (counter == 4) {
                            review?.image4 = fileName
                        }
                        if (counter == 5) {
                            review?.image5 = fileName
                        }
                        file.transferTo(new File(filePath, fileName))
                    }

                }
            }
        }

        def files2 = request?.getFiles("productImages")
        if (files2) {
            for (file in files2) {
                counter++
                if (file && !file.empty) {
                    def matcher = (file.getOriginalFilename() =~ /.*\.(.*)$/)
                    def extension
                    if (matcher.matches()) {
                        extension = String?.valueOf(matcher[0][1])?.toLowerCase()
                    }
                    if (extension == 'jpeg' || extension == 'png' || extension == 'bmp' || extension == 'jpg') {
                        String fileName = UUID.randomUUID().toString() + "." + extension
                        String dir = sourcePath
                        def filePath = new File(dir)
                        filePath.mkdirs()
                        review?.productImage = fileName
                        file.transferTo(new File(filePath, fileName))
                    }
                }
            }
        }
        review?.save(flush: true, failOnError: true)

    }

    def saveProfile(params, request) {
        def profile = Profile?.get(params?.getLong('id')) ?: new Profile()
        profile?.name = params?.name
        profile?.ranking = params?.ranking
        profile?.path = params?.path

        def images = request?.getFiles("image")
        if (images) {
            for (file in images) {
                if (file && !file.empty) {
                    def matcher = (file.getOriginalFilename() =~ /.*\.(.*)$/)
                    def extension
                    if (matcher.matches()) {
                        extension = String?.valueOf(matcher[0][1])?.toLowerCase()
                    }
                    if (extension == 'jpeg' || extension == 'png' || extension == 'bmp' || extension == 'jpg') {
                        String fileName = UUID.randomUUID().toString() + "." + extension
                        String dir = sourcePath
                        def filePath = new File(dir)
                        filePath.mkdirs()
                        profile?.image = fileName
                        file.transferTo(new File(filePath, fileName))
                    }
                }
            }
        }

        profile?.save(flush: true, failOnError: true)

        for (r in profile?.profileReviews) {
            if (!params?.list('review').contains(r.review.id.toString())) {
                ProfileReview.executeUpdate("delete   from ProfileReview pr where pr.review.id=:rid and pr.profile.id=:pid", [rid: r.review.id, pid: profile.id])
            }
        }


        for (r in params?.list('review')) {
            def review = Review?.get(Long.parseLong(r))
            if (!ProfileReview?.findByProfileAndReview(profile, review)) {
                new ProfileReview(profile: profile, review: review)?.save(flush: true, failOnError: true)
            }
        }

    }

    def delete(params) {
        def review = Review?.get(params?.getLong('id'))
        review.delete()
    }

    def saveConfigParams(params) {
        def c1 = ConfigParams?.findByK('title') ?: new ConfigParams(k: 'title', v: '')
        c1?.v = params?.title
        c1.save()

        def c2 = ConfigParams?.findByK('favicon') ?: new ConfigParams(k: 'favicon', v: '')
        c2?.v = params?.favicon
        c2.save()

        def logo = ConfigParams?.findByK('logo') ?: new ConfigParams(k: 'logo', v: '')
        logo?.v = params?.logo
        logo.save()

        def c3 = ConfigParams?.findByK('showPrice') ?: new ConfigParams(k: 'showPrice', v: '0')
        c3?.v = params?.showPrice?.equals('1') ? '1' : '0'
        c3.save()

        for (int i = 1; i < 21; i++) {
            def c4 = ConfigParams?.findByK('navTitle' + i) ?: new ConfigParams(k: 'navTitle' + i, v: '0')
            c4?.v = params?.get('navTitle' + i)
            c4.save()

            def c5 = ConfigParams?.findByK('navLink' + i) ?: new ConfigParams(k: 'navLink' + i, v: '0')
            c5?.v = params?.get('navLink' + i)
            c5.save()
        }

    }

    def deleteProfile(params) {
        def profile = Profile?.get(params?.getLong('id'))
        for (pr in profile.profileReviews) {
            pr.delete()
        }
        profile?.delete()
    }
}
